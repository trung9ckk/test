<?php

namespace App\Imports;

use App\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomerImport implements ToModel , WithHeadingRow
{

    /**
     * @param array $row
     * @return Customer
     */
    public function model(array $row)
    {
        return new Customer([
            'name' => $row['name'] ?? $row['ho_va_ten'],
            'bird' => $row['bird'] ?? $row['nam_sinh'],
            'phone' => $row['phone'] ?? $row['so_dien_thoai'],
            'address' => $row['address'] ?? $row['dia_chi'],
            'status' => $row['status'] ?? $row['trang_thai'],
        ]);
    }
}
