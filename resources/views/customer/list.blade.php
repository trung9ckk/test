@extends('layouts.parent')
@section('content')
    @if(session('success'))
        <div class="text-alert alert-success text-dark text-center py-3 mx-3">{{ session('success') }}</div>
    @else @if(session('error'))
        <div class="text-alert alert-danger text-danger text-center py-3 mx-3">{{ session('error') }}</div>
    @endif
    @endif
    <div id="content" class="container-fluid">
        <div class="card">
            <div class="card-header font-weight-bold d-flex justify-content-between align-items-center">
                <h5 class="m-0 ">Danh sách khách hàng</h5>
                <div class="form-search form-inline" disabled>
                    <form>
                        <input type="text" value="{{ request()->search }}" name="search" class="form-control form-search" placeholder="Tìm kiếm">
                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div class="analytic">
                    <a href="{{ route('customer.index') }}" class="text-primary">Tất cả<span class="text-muted">({{ $count_status_customers[0] }})</span></a>
                    <a href="{{ request()->fullUrlWithQuery(['status' => 'customer_new']) }}" class="text-primary">Khách hàng mới<span class="text-muted">({{ $count_status_customers[1] }})</span></a>
                    <a href="{{ request()->fullUrlWithQuery(['status' => 'call_success']) }}" class="text-primary">Đã liên hệ thành công<span class="text-muted">({{ $count_status_customers[2] }})</span></a>
                    <a href="{{ request()->fullUrlWithQuery(['status' => 'call_no_start'])}}" class="text-primary">Gọi điện không bắt máy<span class="text-muted">({{ $count_status_customers[3] }})</span></a>
                    <a href="{{ request()->fullUrlWithQuery(['status' => 'call_no']) }}" class="text-primary">Không liên hệ được<span class="text-muted">({{ $count_status_customers[4] }})</span></a>
                </div>
                <div class="form-action form-inline py-3 d-flex justify-content-between">
                    <div class="select">
                        <select class="form-control mr-1" id="">
                            <option>Chọn</option>
                            <option>Tác vụ 1</option>
                            <option>Tác vụ 2</option>
                        </select>
                        <input type="submit" name="btn-search" value="Áp dụng" class="btn btn-primary">
                    </div>
                    <div class="import">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <label for="user-file">
                                <div class="btn sbold green"> Add
                                    <i class="fa fa-plus"></i>
                                </div>
                            </label>
                            <input id="user-file" type="file" name="user_file" accept=".xlsx, .xls, .csv, .ods">
                            <button class="btn-secondary" type="submit">Import</button>
                        </form>
                    </div>
                </div>
                <table class="table table-striped table-checkall">
                    <thead>
                    <tr>
                        <th scope="col">
                            <input name="checkall" type="checkbox">
                        </th>
                        <th scope="col">#</th>
                        <th scope="col">Họ và tên</th>
                        <th scope="col">Năm sinh</th>
                        <th scope="col">Số điện thoại</th>
                        <th scope="col">Địa chỉ</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Tác vụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($list_customer->count() > 0)
                        @foreach($list_customer as $key => $customer)
                            <tr>
                                <td>
                                    <input type="checkbox">
                                </td>
                                <td scope="row">{{ $key + 1 }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->bird }}</td>
                                <td>{{ $customer->phone }}</td>
                                <td>{{ $customer->address }}</td>
                                <td>
                                    @if($customer->status == 1)
                                        Khách hàng mới
                                    @else @if($customer->status == 2)
                                        Đã liên hệ thành công
                                    @else @if($customer->status == 3)
                                            Gọi điện không bắt máy
                                        @else @if($customer->status == 4)
                                                Không liên hệ được
                                            @endif
                                        @endif
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip"
                                            data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip"
                                            data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" class="text-center font-weight-bold">Không tìm thấy khách hàng</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    {{ $list_customer->links() }}
                </nav>
            </div>
        </div>
    </div>
@endsection
