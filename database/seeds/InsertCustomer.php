<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsertCustomer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'test', 'email' => 'admin@gmail.com', 'password' => \Illuminate\Support\Facades\Hash::make('123456')],
        ]);
    }
}
