<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Imports\CustomerImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $list_customer = null;
        $status_customer = $request->input('status');
        $keyword = $request->input('search') ? $request->input('search') : '';
        switch ($status_customer) {
            case 'customer_new':
                $list_customer = Customer::where('status' , 1)->paginate(20);
                break;
            case 'call_success':
                $list_customer = Customer::where('status' , 2)->paginate(20);
                break;
            case 'call_no_start':
                $list_customer = Customer::where('status' , 3)->paginate(20);
                break;
            case 'call_no':
                $list_customer = Customer::where('status' , 4)->paginate(20);
                break;
            default:
                $list_customer = Customer::where('name' , 'like' ,"%$keyword%")
                    ->orWhere('phone' , 'like' ,"%$keyword%")
                    ->orWhere('address' , 'like' ,"%$keyword%")
                    ->paginate(20);
        }
        $count_status_customers = [
            Customer::count(),
            Customer::where('status' , 1)->count(),
            Customer::where('status' , 2)->count(),
            Customer::where('status' , 3)->count(),
            Customer::where('status' , 4)->count(),
        ];
        return view('customer.list', compact('list_customer', 'count_status_customers'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */

    public function import()
    {
        if(!request()->file('user_file'))
        {
            return redirect()->back()->with('error', 'File không được để trống');
        }
        $import = Excel::import(new CustomerImport(), request()->file('user_file'));
        return $import ? redirect()->back()->with('success', 'Import thành công') : redirect()->back()->with('error', 'Import thất bại');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
